package com.badgerhammer.string;

/**
 * ...
 * @author Nick Holder
 */

class BHStringTools 
{

	static public function startsWithNumber( string:String ):Bool 
	{
		var firstCharacter:String = string.substr(0, 1);
		for ( i in 0...9 )
		{
			if ( firstCharacter == Std.string( i ) )
			{
				return true;
			}
		}
		return false;
	}
	
	
}