package com.badgerhammer.tinytextworlds;
import com.badgerhammer.string.BHStringTools;

/**
 * ...
 * @author Nick Holder
 */

class TTWXmlTools 
{
	static public function doAttributeComparison( worldXmlRef:WorldXml , allowedValuesString:String  , targetAttribute:String) 
	{
		if ( allowedValuesString == "" ) return false;
		if ( allowedValuesString == "*" ) return true;
		
		var allowedValues:Array<String> = allowedValuesString.split("&");
		
		var foundVisibleState:Bool = false;
		
		
		
		for ( i in 0...allowedValues.length )
		{			
			// state is given in the form <objectId>:<requiredState>
			var allowedValueInfo:Array<String> = allowedValues[i].split(":");
			if ( allowedValueInfo.length < 2 ) 
			{
				trace( "Script Error! AllowedValues must be * or \"\" or object:value. Looking for \"" + allowedValuesString + "\"");
			}
			var currentValue:String =  worldXmlRef.getObject( allowedValueInfo[0]).get( targetAttribute ) ;
			var allowedValue:String = allowedValueInfo[1];
			var firstCharacter:String = allowedValue.substr(0, 1);
			
			if ( firstCharacter == "<" || firstCharacter == ">")
			{
				if ( !greterThanOrLessThanComparison( currentValue , firstCharacter ,  allowedValue.substr(1) ) )
				{
					return false;
				}
			}
			else if ( allowedValue != currentValue )
			{
				return false;
			}
		}
		return true;
	}
	
	static public function acceptableStartValuesComparison( currentValue:String , allowedValue:String  ) 
	{
		if ( allowedValue == "*" ) return true;
		
		var firstCharacter:String = allowedValue.substr(0, 1);
		if ( firstCharacter == "<" || firstCharacter == ">")
		{
			return greterThanOrLessThanComparison( currentValue , firstCharacter ,  allowedValue.substr(1) );
		}
		else if ( allowedValue == currentValue )
		{
			return true;
		}
		return false;
	}
	
	static private function greterThanOrLessThanComparison( currentValue:String, firstCharacter:String, allowedValue:String)
	{
		if ( BHStringTools.startsWithNumber( allowedValue ) 
			  && BHStringTools.startsWithNumber( currentValue ) )
		{
			
			
			if ( firstCharacter == ">" )
			{
				if ( Std.parseFloat( currentValue)  > Std.parseFloat( allowedValue ) ) 
				{
					return true;
				}
			}
			else
			{
				if ( Std.parseFloat( currentValue)  < Std.parseFloat( allowedValue ) )
				{
					return true;
				}
			}
		}
		
		return false;
	}
}