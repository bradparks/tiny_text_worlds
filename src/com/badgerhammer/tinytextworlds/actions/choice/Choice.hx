 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.actions.choice;

import com.badgerhammer.tinytextworlds.string.TTWStringTools;
import com.badgerhammer.tinytextworlds.TTWXmlTools;
import com.badgerhammer.tinytextworlds.WorldXml;

/**
 * ...
 * @author Nick Holder
 */

class Choice 
{
	public var text( default , null ):String;
	public function setText( newtext:String ):Void { text = newtext; }
	

	public var choices( default , null ):Array<String>;
	public function addChoice( newChoice:String ):Void { choices.push( newChoice ); }
	
	
	public var actions( default , null ):Array<Xml>;
	public function addAction( newAction:Xml ):Void { actions.push( newAction ); }
	
	
	public var targetObjects( default , null ):Array<String>;
	public function addTargetObject(  newTargetObject:String ):Void { targetObjects.push( newTargetObject ); }
	
	public var populatedChoiceFound:Bool;
	
	
	public function new( choiceSteps:Iterator<Xml> , worldXml:WorldXml , currentObject:Xml) 
	{
		choices = new Array<String>();
		actions = new Array<Xml>();
		targetObjects = new Array<String>();
		
		populatedChoiceFound = false;
		
		// iterate through steps
		for ( xml in choiceSteps )
		{
			// text
			if ( xml.nodeName == "text" )
			{
				setText( TTWStringTools.regExpString( xml.firstChild().toString() , "__objectName__" ,  currentObject.get("name") ) );
			}
			// OPTION
			else if ( xml.nodeName == "option"  )
			{
				var nodeType:String = xml.get( "type" );
				
				// Add the choice text
				
				// Object
				if ( nodeType == "object" )
				{
					var targetObjectId:String = xml.get( "targetObjectId" ); // the object the action is on				
					var targetActionId:String = xml.get( "actionId" );
				
					var visibleStates:Array<String> = xml.get( "visibleStates" ).split(" "); 
				
					var choosableStateFound:Bool = false;
					
					for ( state in visibleStates )
					{
						if ( TTWXmlTools.doAttributeComparison( worldXml , state , "state") )
						{
							choosableStateFound = true;
							break;
						}
					}
					
					// Found a choosable state match?
					if ( !choosableStateFound )
					{
						continue;
					}
					
					
					// it's an object so get the object name from the objects xml
					if ( xml.get( "text" ).substr(0,13) == "nameOfObject:" )
					{
						var choiceObjectId:String = xml.get( "text" ).split(":")[1]; // the object we present as a choice
					
						var choiceObject = worldXml.getObject( choiceObjectId );
						addChoice( choiceObject.get( "name" ) );
					}
					else
					{
						addChoice(  xml.get( "text" ) );
					}
					
					// Add the coreesponding action to the choice
					addAction( worldXml.getObjectAction( targetObjectId , targetActionId )  );	
					
					// Add the objcet the function is to be called on
					addTargetObject( targetObjectId );
					
					if ( choosableStateFound )
					{
						populatedChoiceFound = true;
					}
				}
				
			}	
		}
	}
}