 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.actions;

import com.badgerhammer.tinytextworlds.actions.choice.SayChoice;
import com.badgerhammer.tinytextworlds.TTWXmlTools;
import com.badgerhammer.tinytextworlds.WorldXml;
import com.badgerhammer.tinytextworlds.actions.choice.Choice;
import com.badgerhammer.tinytextworlds.string.TTWStringTools;
import com.badgerhammer.xml.BHXmlTools;
import msignal.Signal;

/**
 * ...
 * @author Nick Holder
 */

class ActionResolver 
{
	private var _worldXml:WorldXml;
	private var _refreshPlaceCallback:Void -> Void;

	private var _currentObject:Xml;
	private var _currentAction:Xml;
	
	private var _currentSayChoice:SayChoice; // A choice used in the Say display with a query and selectable options

	// Safety checks
	private var _resolvedActions:Array<ActionInfo>;
	private var _haveMadeChoice:Bool;
	
	
	// Signal
	public var foundSayInActions:Signal1<SayChoice>;
	public var allActionsCompleted:Signal0;
	public var placeRefreshRequested:Signal0;
	
	
	// CONSTRUCTOR
	public function new( worldXml:WorldXml ) 
	{
		_worldXml = worldXml;
		
		//_resolvedActions = new Array<ActionInfo>();
		
		
		foundSayInActions = new Signal1();
		allActionsCompleted = new Signal0();
		placeRefreshRequested = new Signal0();
	}
	
	// Reset the list of resolved actions
	public function clearChecks():Void
	{
		 _haveMadeChoice = false; 
		
		/* Anti Loop check
		while ( _resolvedActions.length != 0 )
		{
			_resolvedActions.shift();
		}
		*/
	}
	
	
	
	// Resolve an action
	public function resolve(  objectID:String , actionId:String ):Void
	{
		
		// Locate the action on the specifed object with the correct id
		findAction( objectID , actionId );
		
		// process the located action
		processActionSteps( _currentAction );
	}
	
	
	
	// Start by finding the action with a matchin id
	private function findAction(  objectID:String , actionId:String ):Void
	{
		
		
		_currentObject = _worldXml.getObject( objectID );
		
		_currentAction = _worldXml.getObjectAction( objectID , actionId);
		
	}
	
	
	
	// Work through the steps in the action
	private function processActionSteps( action:Xml ):Void
	{
		/* TODO check that same action is not run twice or throw error
		 * Not currently needed as actions can't call actions
		 * If ActionTriggers in states are implemented this will be required
		_resolvedActions.push( new ActionInfo(  _currentAction.get("id") , 
												_currentAction.get("name") , 
												false , 
												_currentObject.get( "id" ) ) );
		
		trace( _resolvedActions );
		*/
		
		var actionSteps:Iterator<Xml> = action.elements();
		
		var changedWorld:Bool = false;
		
		
		// iterate through steps
		for ( xml in actionSteps )
		{
			if ( xml.nodeName == "set"  )
			{
				doSet(xml);
				changedWorld = true;
			}
			// SAY
			else if ( xml.nodeName == "say"   )
			{
				// We make a choice for what we want to say
				// it does not need to have actions, but if they are present the place will
				// be hidden until the user chooses an option
				_currentSayChoice = new SayChoice( xml.elements() , _worldXml , _currentObject );
				
				foundSayInActions.dispatch( _currentSayChoice );
				
				if ( _currentSayChoice.choices.length == 0 )
				{
					_currentSayChoice = null;
				}
			}
			
		}
		
		// Are there any incomplete choices? or sayChoices
		// if not all actionas are complete
		if ( _currentSayChoice == null) 
		{	
			// if not all actions and choices are complete.
			allActionsCompleted.dispatch();
			placeRefreshRequested.dispatch();
			
			// Clear loop and choice checks
			clearChecks();
			
			// return to avoid calling dispatching placeRefreshRequested twice below
			return;
		}
		
		
		// have we changed the world if so refresh the place
		// we must check there is no _currentSayChoice to avoid updating whilst place is hidden
		if ( changedWorld && _currentSayChoice == null )
		{
			placeRefreshRequested.dispatch();
		}
	}
	
	
	
	// Perform a set comand on the WorldXml
	private function doSet( setXml:Xml ):Void
	{
		var targetObject:Xml;
		if ( setXml.get( "targetObjectId" ) == "__player__" )
		{
			targetObject = _worldXml.getPlayer();
		}
		else
		{
			targetObject = _worldXml.getObject( setXml.get("targetObjectId") );
			targetObject.set( "hasChanged" , "true" );
		}
		
		
		// Check to see if the target atrribute has a value that we want to change
		// we only change the attribute if we find a match or a wildcard
		var acceptableStartValues:Array<String> = setXml.get( "acceptableStartValues" ).split( " " );
		
		
		var acceptableValueFound:Bool = false;
		
		var currentAttributeValue:String = targetObject.get( setXml.get( "targetAttribute" ));
		
		for ( acceptableValue in acceptableStartValues )
		{
			acceptableValueFound = TTWXmlTools.acceptableStartValuesComparison(  currentAttributeValue , acceptableValue);
			// If true break the loop
			if ( acceptableValueFound ) break;
		}
		
		
		if ( acceptableValueFound  )
		{
			var firstCharacter:String = setXml.get( "newValue" ).substr(0, 1);
			
			if ( firstCharacter == "+" || firstCharacter == "-")
			{
				BHXmlTools.doArithmeticOnAttribute( targetObject , setXml.get( "targetAttribute" ) , firstCharacter , setXml.get( "newValue" ).substr(1) );
			}
			else
			{
				// Set the "targetObject" 's "targetAttribute" to the "newValue"
				targetObject.set( setXml.get( "targetAttribute" ) , setXml.get( "newValue" ) );
			}
			
		}
		
		
	}
	
	
	// The user has clicked a choice in the SayDisplay do the action it refers to
	public function doSayChoice( choiceIndex:Int ):Void
	{		
		var currentSayChoice:SayChoice = _currentSayChoice;
		_currentSayChoice = null;
		
		
		// set the current object to the object whos function we are calling - added for regExp of __objectName__
		_currentObject = _worldXml.getObject( currentSayChoice.targetObjects[choiceIndex] );
		processActionSteps( currentSayChoice.actions[choiceIndex] );
	}
}