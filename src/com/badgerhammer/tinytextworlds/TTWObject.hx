 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds;

import com.badgerhammer.tinytextworlds.WorldXml;
import com.badgerhammer.xml.BHXmlTools;
import com.badgerhammer.tinytextworlds.actions.ActionInfo;

/**
 * ...
 * @author Nick Holder
 */

class TTWObject 
{
	
	private var _xml:Xml;
	private var _currentState:String;
	private var _currentStateXml:Xml;
	private var _worldXmlRef:WorldXml;
	
	public function new( worldXmlRef:WorldXml , xml:Xml) 
	{
		_worldXmlRef = worldXmlRef;
		
		_xml = xml;
		
		_currentState = _xml.get( "state" );
		_currentStateXml = BHXmlTools.findNodeByAttributeValue( _xml.elementsNamed( "state" ) , "id" , _currentState );
	}
	
	
	// Get the text for the place including all the text for contained objects
	public function getTextBlocks():Array<TextBlock>
	{
		var textBlocks = new Array<TextBlock>();
		
		// Make Text Blocks for Object Text
		_currentStateXml = BHXmlTools.findNodeByAttributeValue( _xml.elementsNamed( "state" ) , "id" , _currentState );
		
		// Has this objects state been changed since last display?
		var hasChanged:Bool  = false;
		
		// there is no hasChanged attribute by default so only check to see if it == "true"
		if ( _xml.get( "hasChanged" ) == "true" )
		{
			hasChanged = true;
			
			// set the hasChanged attribute
			_xml.set( "hasChanged" , "false" );
		}

		var preNameText:TextBlock = new TextBlock( TextBlock.NORMAL , 
												   _currentStateXml.elementsNamed( "preNameText" ).next().firstChild().toString() ,
												   hasChanged );
		
		var name:TextBlock = new TextBlock( TextBlock.OBJECT , 
											_xml.get( "name" ) ,
											_xml.get( "id" ),
											hasChanged,
											getObjectActions());
		
		var postNameText:TextBlock = new TextBlock( TextBlock.NORMAL , 
												   _currentStateXml.elementsNamed( "postNameText" ).next().firstChild().toString() ,
												   hasChanged );
		
		textBlocks.push( preNameText );
		textBlocks.push( name );
		textBlocks.push( postNameText );
		
		return textBlocks;
	}
	
	
	
	// Look for visible options for this object
	private function getObjectActions():Array<ActionInfo>
	{
		var objectActions:Array<ActionInfo> = new Array<ActionInfo>();
		
		
		// Iterate through actions
		for ( action in _xml.elementsNamed( "action" ) )
		{
			// check this action is visible
			if ( actionVisible( action ) )
			{
				// add to objectActions				
				
				// Does the action have a choice? Actions with choices will be styled differently
				var hasChoice:Bool = false;
				
				if ( action.elementsNamed( "choice" ).hasNext() )
				{
					hasChoice = true;
				}
				
				objectActions.push( new ActionInfo( action.get( "id" ) , action.get( "name" ) ,  hasChoice ) );
			}
			
		}
		return objectActions;
	}
	
	
	
	// public function for determining if the object is visible
	public function objectVisible():Bool
	{
		
		if ( _currentStateXml.get( "visible" ) == "false" )
		{
			return false;
		}
		
		return true;
	}
	
	
	
	// Check the visible states against the objects current state and return a Bool
	private function actionVisible( xml:Xml ):Bool
	{
		
		var visibleStates:Array<String> = xml.get( "visibleStates" ).split(" ");
		
		
		// TODO: Add more state filtering
		if ( visibleStates.length == 0 )
		{
			return false;
		}
		
		// Look for matching visible states
		for ( visibleState in visibleStates )
		{
			if ( TTWXmlTools.doAttributeComparison( _worldXmlRef , visibleState , "state") )
			{
					return true;
			}
			
		}
		
		return false;
	}
}