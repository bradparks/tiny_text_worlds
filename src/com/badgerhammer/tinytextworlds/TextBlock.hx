 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds;

import com.badgerhammer.tinytextworlds.actions.ActionInfo;

/**
 * ...
 * @author Nick Holder
 */


 
class TextBlock 
{
	// TextBlock Types
	public inline static var NORMAL:String = "normal";
	public inline static var PLACE:String = "place";
	public inline static var OBJECT:String = "object";
	
	
	// Vars

	public var type( default , null ):String;
	
	public var actions( default , null ):Array<ActionInfo>;

	public var text( default , null ):String;

	public var id( default , null ):String;

	public var hasChanged( default , null ):Bool; // read only hasChanged when an objects state has been changed
	
	
	//
	public function new( type:String , text:String , id:String = "" , hasChanged:Bool = false , actions:Array<ActionInfo> = null ) 
	{
		this.type = type;
		this.text = text;
		this.id = id;
		this.actions = actions;
		this.hasChanged = hasChanged;
	}
	
	
	
	
}