 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.display.javascript;

import com.badgerhammer.tinytextworlds.actions.choice.Choice;
import com.badgerhammer.tinytextworlds.actions.choice.SayChoice;
import com.badgerhammer.tinytextworlds.TextBlock;
import com.badgerhammer.tinytextworlds.actions.ActionInfo;
import msignal.Signal;
/**
 * ...
 * @author Nick Holder
 */

class Display implements IDisplay
{
	
	private var _sayDisplay:SayDisplay;
	private var _placeDisplay:PlaceDisplay;
	private var _actionDisplay:ActionDisplay;
	
	public var actionClicked:Signal2<String,String>;
	public var sayChoiceClicked:Signal1<Int>;
	
	public function new(  actionPromptText:String ,  cancelText:String) 
	{
		actionClicked = new Signal2( );
		sayChoiceClicked = new Signal1( );
		
		// Make action display first so we can pass it's display function to  Place Display
		_actionDisplay = new ActionDisplay(  actionPromptText , cancelText , clear );
		_actionDisplay.actionClicked.add( onActionClicked );
		
		_placeDisplay = new PlaceDisplay( displayActions);
		
		_sayDisplay = new SayDisplay(  );
		_sayDisplay.sayChoiceClicked.add( onSayChoiceClicked );
	}
	
	private function onActionClicked( objectId:String , actionId:String)
	{
		actionClicked.dispatch(objectId , actionId);
	}
	
	private function onSayChoiceClicked( sayChoiceId:Int )
	{
		sayChoiceClicked.dispatch( sayChoiceId );
	}
	
	
	// Display all the place TextBlocks
	public function say( choice:SayChoice ):Void
	{
		_sayDisplay.display( choice );
		
		// If the sayCHoice has actions then clear the place until an action is chosen
		if ( _sayDisplay.hasActions )
		{
			_placeDisplay.clear();
			_actionDisplay.clear();
		}
		
	}
	
	
	
	// Display all the place TextBlocks
	public function displayPlace( placeTextBlocks:Array<TextBlock> ):Void
	{
		_placeDisplay.displayPlace( placeTextBlocks );
	}
	
	// Display a list of possible actions
	public function displayActions( actions:Array<ActionInfo> , objectId:String , objectName:String):Void
	{
		// TODO:Recolour rather than clear _sayDisplay
		_sayDisplay.clear();
		_actionDisplay.displayActions( actions , objectId , objectName );
	}
	
	// Clear all the 
	public function clear( clearActionDisplay:Bool , clearPlaceDisplay:Bool  , clearSayDisplay:Bool ):Void
	{
		if ( clearActionDisplay )
		{
			_actionDisplay.clear();
		}
		if ( clearPlaceDisplay )
		{
			_placeDisplay.clear();
		}
		if ( clearSayDisplay )
		{
			_sayDisplay.clear();
		}
	}
	

}