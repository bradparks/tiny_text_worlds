 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.display.javascript;

import js.Lib;
import js.Dom;

import com.badgerhammer.tinytextworlds.actions.ActionInfo;
import msignal.Signal;
/**
 * ...
 * @author Nick Holder
 */

class ActionDisplay 
{
	private var _ttwActionsDiv:HtmlDom;
	private var _interactiveDomElements:Array<HtmlDom>;
	private var _objectId:String;
	
	private var _actionPromptText:String;
	private var _cancelText:String;
	private var _useActionPrompt:Bool;
	
	private var _clearCallback:Bool -> Bool -> Bool -> Void;
	
	public var actionClicked:Signal2<String,String>;
	
	public function new( actionPromptText:String , 
						cancelText:String , 
						clearCallback:Bool -> Bool -> Bool -> Void) 
	{
		_clearCallback = clearCallback;
		
		actionClicked = new Signal2( );
		
		_actionPromptText = actionPromptText;
		_cancelText = cancelText;
		
		if ( _actionPromptText != "" )
		{
			_useActionPrompt = true;
		}
		else
		{
			_useActionPrompt = false;
		}
		
		// Find the div tag to write to
		_ttwActionsDiv = Lib.document.getElementById( "TTWActions" );
		
		_interactiveDomElements = new Array<HtmlDom>();
	}
	
	
	// Display all the action titles
	// objectId = object or place ID
	public function displayActions( actions:Array<ActionInfo> , objectId:String , objectName:String  ):Void
	{
		
		
		_objectId = objectId;
		
		// Clear old action display
		clear();
		
		// Add a horizontal rule
		_ttwActionsDiv.appendChild( Lib.document.createElement( "hr" ) );
		
		// iterate through actions creating elements
		var element:HtmlDom;
		
		
		// Are we using the action prompt?
		if ( _useActionPrompt)
		{
			// First add the text prompt e.g. choose an action for __objectName__
			element = Lib.document.createElement("span");
			element.id = "actions-prompt" ;
			element.innerHTML = _actionPromptText + " "  + objectName + "<br/>";
			// Add to HTML
			_ttwActionsDiv.appendChild( element ) ;
		}
		
		
		for ( index in 0 ... actions.length )
		{
			if ( index != 0 )
			{
				element = Lib.document.createElement("br");
				_ttwActionsDiv.appendChild( element ) ;
			}
			
			// Make a new element for this place/object
			element = Lib.document.createElement("a");
			element.id = "action-" + actions[index].actionId ;
			element.className = "action interactive";
			element.innerHTML = actions[index].actionName;
			
			if ( actions[ index ].hasChoice )
			{
				
				element.setAttribute( "href" , "#TTWChoice" );
				element.innerHTML += "...";
			}
			else
			{
				element.setAttribute( "href" , "#TTWSay" );
			}
			
			_ttwActionsDiv.appendChild( element ) ;
			
			// Has actions so add an onclick
			element.onclick = action_onClick;
			
			// Store all elements to remove their listeners
			_interactiveDomElements.push( element );
		}
		
		
		// Break
		element = Lib.document.createElement("br");
		_ttwActionsDiv.appendChild( element );
		
		// Add the cancel button
		element = Lib.document.createElement( "a" );
		element.setAttribute( "href" , "#TTWSay" );
		element.innerHTML = _cancelText;
		element.className = "interactive cancel";
		element.id = "cancel-action";
		_ttwActionsDiv.appendChild( element );
		element.onclick = cancel_onClick;
		
		// Store cancel element to remove it's listeners
		_interactiveDomElements.push( element );

	}
	
	
	// Cancel click event handler
	private function cancel_onClick( e : Event ):Void
	{
		_clearCallback( true, false , false  ) ;
	}
	
	
	
	// Click event handler
	private function action_onClick( e : Event ):Void
	{	
		// Reset all action buttons to not clicked state
		for ( element in _interactiveDomElements  )
		{
			if ( element.id != "cancel-action" )
			{
				element.className = "action interactive";
			}
			
		}
		
		// Set this action button as clicked
		e.target.className = "action interactive action-clicked";
		
		// get the actionID
		var actionId:String =  e.target.id.split("-")[1];
		
		// Do the action
		actionClicked.dispatch( _objectId ,  actionId ); 
	}
	
	
	
	// Clear the div and all listeners on objects in it
	public function clear():Void
	{
		removeListeners();
		_ttwActionsDiv.innerHTML = "";
	}
	
	
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
		while ( _interactiveDomElements.length > 0 )
		{
			_interactiveDomElements.shift().onclick = null;
		}
	}
}