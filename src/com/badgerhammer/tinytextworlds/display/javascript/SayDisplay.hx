 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.display.javascript;

import com.badgerhammer.tinytextworlds.actions.choice.SayChoice;
import com.badgerhammer.tinytextworlds.string.TTWStringTools;

import js.Lib;
import js.Dom;
import msignal.Signal;

/**
 * ...
 * @author Nick Holder
 */

class SayDisplay 
{
	private var _ttwSayDiv:HtmlDom;
	private var _interactiveDomElements:Array<HtmlDom>;
	
	public var hasActions( default , null ):Bool;
	
	public var sayChoiceClicked:Signal1<Int>;
	
	public function new( ) 
	{
		sayChoiceClicked = new Signal1( );
		
		// Find the div tag to write to
		_ttwSayDiv = Lib.document.getElementById( "TTWSay" );
		
		_interactiveDomElements = new Array<HtmlDom>();
	}
	
	
	
	// Display a new choice
	public function display( choice:SayChoice ):Void
	{
		// Clear old action display
		clear();
		
		var element:HtmlDom;
		
		// First add the query
		element = Lib.document.createElement("span");
		element.id = "choice-query" ;
		element.innerHTML = TTWStringTools.stripCDataEnclosure( choice.text ) + "<br/>";
		// Add to HTML
		_ttwSayDiv.appendChild( element ) ;
		
		var choiceCount:Int = choice.choices.length;
		
		// Are there any actions in this sayChoice?
		hasActions = false;
		
		if ( choiceCount > 0 )
		{
			hasActions = true;
		}
		
		// iterate through actions creating elements
		for ( index in 0 ... choiceCount )
		{
			if ( index != 0 )
			{
				element = Lib.document.createElement("br");
				_ttwSayDiv.appendChild( element ) ;
			}
			
			// Make a new element for this place/object
			element = Lib.document.createElement("a");
			element.setAttribute( "href" , "#TTWSay");
			element.id = "choice-" + index ;
			element.className = "action interactive";
			element.innerHTML = choice.choices[index];
			
			
			
			_ttwSayDiv.appendChild( element ) ;
			
			// Has actions so add an onclick
			element.onclick = choice_onClick;
			
			// Store all elements to remove their listeners
			_interactiveDomElements.push( element );
		}
		
		
		// Jump window to TTWActions div;
		//Lib.window.location.hash =  "TTWSay";
	}
	
	
	
	// Click event handler
	private function choice_onClick( e : Event ):Void
	{	
		var choiceIndex:Int =  Std.parseInt( e.target.id.split("-")[1] );
		clear();
		sayChoiceClicked.dispatch( choiceIndex ); 
	}
	
	
	
	// Clear the div and all listeners on objects in it
	public function clear():Void
	{
		removeListeners();
		_ttwSayDiv.innerHTML = "";
	}
	
	
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
		while ( _interactiveDomElements.length > 0 )
		{
			_interactiveDomElements.shift().onclick = null;
		}
	}
}