 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.display.javascript;

import com.badgerhammer.tinytextworlds.TextBlock;
import com.badgerhammer.tinytextworlds.actions.ActionInfo;

import js.Lib;
import js.Dom;

/**
 * ...
 * @author Nick Holder
 */

class PlaceDisplay 
{

	private var _interactiveDomElements:Array<HtmlDom>;
	private var _ttwPlaceDiv:HtmlDom;
	public var _displayActionFunction:Array<ActionInfo> -> String -> String -> Void;
	public var _placeTextBlocks:Array<TextBlock>;

	
	public function new( displayActionFunction:Array<ActionInfo> -> String -> String -> Void ) 
	{
		_displayActionFunction = displayActionFunction;
		
		// List interactive DOM elements to remove click listeners later
		_interactiveDomElements = new Array<HtmlDom>();
		
		// Find the div tag to write to
		_ttwPlaceDiv = Lib.document.getElementById( "TTWPlace" );
		
	}

	// Display all the place TextBlocks
	public function displayPlace( placeTextBlocks:Array<TextBlock> ):Void
	{
		clear();
		
		// Store placeTextBlocks
		_placeTextBlocks = placeTextBlocks;
		
		var firstSentence:Bool = true;
		var createdTextBlocks:Int = 0;
		
		// iterate through all the TextBlocks in placeTextBlocks
		for ( textBlock in placeTextBlocks )
		{
			
			var element:HtmlDom;
			
			// If the TextBlock has an Id
			// It is a place or an object
			
			
			if ( textBlock.id != "" )
			{
				// Does the textblock have any actions?
				if ( textBlock.actions.length > 0 )
				{
					// Make a new element for this place/object
					element = Lib.document.createElement("a");
					// Has actions so add an onclick
					element.onclick = interactiveDomElement_onClick;
					
					// Keep track of all the elements with listeners
					_interactiveDomElements.push( element );
					
					element.className = textBlock.type;
					element.className += "-has-actions interactive";
					
					element.setAttribute( "href" , "#TTWActions" );
				}
				else
				{
					// Make a new element for this place/object
					element = Lib.document.createElement("span");
				
					// Add -no-actions to the classname for CSS
					element.className = textBlock.type;
					element.className += "-no-actions interactive";
				}
				
				element.id = textBlock.id;
				element.innerHTML = textBlock.text;
			}
			else
			// This is a standard text block
			{
				element = Lib.document.createElement("span");
				element.className = textBlock.type;
				
				
				
				if ( firstSentence )
				{
					element.innerHTML = "<span class=\"first-letter\">" + textBlock.text.charAt( 0 ) + "</span>";
					element.innerHTML += textBlock.text.substr( 1 );
					
					firstSentence = false;
				}
				else
				{
					element.innerHTML = textBlock.text;
				}
			}
			
			// Has this text changed since the last time the user saw it?
			if ( textBlock.hasChanged )
			{
				element.className += " has-changed";
			}
			
			createdTextBlocks ++;
			
			if ( createdTextBlocks == 3 )
			{
				createdTextBlocks = 0;
				element.innerHTML += " ";
			}
			
			// Add to the text in the place div
			_ttwPlaceDiv.appendChild( element );	
		}	

	}

	
	
	// Reset the classes of clicked buttons
	private function resetClickedButtons():Void
	{
		// Reset all object buttons to not clicked state
		for ( element in _interactiveDomElements  )
		{
			var classes:Array<String> = element.className.split(" ");
			
			element.className = "";
			
			// Iterate through classNames re-adding but excluding object-clicked
			for ( className in classes )
			{
				if ( className == "object-clicked" )
				{
					continue;
				}
				
				element.className += className +" ";
			}
			 
		}
	}
	
	
	
	// Click event handler
	private function interactiveDomElement_onClick( e : Event ):Void
	{	
		resetClickedButtons();
		
		// Set this action button as clicked
		e.target.className += " object-clicked";
		
		
		var actionsArray:Array<ActionInfo>  =  new Array<ActionInfo> ( );
		var textBlockId:String = "";
		var textBlockName:String = "";
		
		// Match event id to a textBlock id
		for ( textBlock in _placeTextBlocks )
		{
			if ( textBlock.id == e.target.id )
			{
				// Get the TextBlock's actions
				actionsArray = textBlock.actions;
				textBlockId = textBlock.id;
				textBlockName = textBlock.text ;
				break;
			}
		}
		
		// display the actions, included the textblockID so we can callback to GameLogic for action resolution
		_displayActionFunction( actionsArray , textBlockId , textBlockName);
	}
	
	
	
	// Clear the div and all listeners on objects in it
	public function clear():Void
	{
		removeListeners();
		_ttwPlaceDiv.innerHTML = "";
	}
	
	
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
		while ( _interactiveDomElements.length > 0 )
		{
			_interactiveDomElements.shift().onclick = null;
		}
	}
	
	
	/* OLD EVENT CODE
	// Add a listener to an interactive DOM element
	private function addListener( domElement:HtmlDom ):Void
	{
		untyped
		{    
			 if ( js.Lib.isIE )
			{
				domElement.attachEvent( "click", interactiveDomElement_onClick, false );
			} else {
				domElement.addEventListener( "click", interactiveDomElement_onClick, false );
			}
		}
	}
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
			
			untyped
			{
				if ( js.Lib.isIE )
				{
					interactiveDomElements.shift().detachEvent( "click", interactiveDomElement_onClick );
				} else {
					interactiveDomElements[0].removeEventListener( "click", interactiveDomElement_onClick );
					interactiveDomElements.shift();
				}
			}
			
		}
		
	}
	*/
}