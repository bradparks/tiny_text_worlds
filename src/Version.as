package 
{
  public final class Version
  {
      public static const Major:int = 1;
      public static const Minor:int = 0;
      public static const Build:int = 432;
      public static const Revision:int = 0;
      public static const Timestamp:String = "12/02/2013 09:56:33";
      public static const Author:String = "Nick Holder";
  }
}